:orphan:

.. _about:

About us
========

.. include:: ../AUTHORS.rst

.. seealso::

   :ref:`How you can contribute to the project <contributing>`

.. _citing-batman:

Citing batman
-------------

If you use batman in a scientific publication, we would appreciate
citations to one of the following paper:

  `Comparison of Polynomial Chaos and Gaussian Process surrogates for
  uncertainty quantification and correlation estimation of spatially
  distributed open-channel steady flows <http:// .html>`_, Roy, P.T.
  *et al.*, SERRA, 2017.

  Bibtex entry::

    @article{batman,
     title={Comparison of Polynomial Chaos and Gaussian Process surrogates for
     uncertainty quantification and correlation estimation of spatially
     distributed open-channel steady flows},
     author={Roy, P.T. and El Moçaïd, N. and Ricci, S. and Jouhaud, J.-C. and
     Goutal, N. and De Lozzo, M. and Rochoux M.C.},
     journal={Stochastic Environmental Research and Risk Assessment},
     year={2017}
    }